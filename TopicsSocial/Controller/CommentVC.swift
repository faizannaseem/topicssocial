//
//  CommentVC.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase
import OneSignal
import SDWebImage

class CommentVC: UIViewController,Alertable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var topicLabel: UILabel!
    @IBOutlet weak var topicVotesLabel: UILabel!
    
    var topicObj : DataSnapshot!
    var commentArray = [DataSnapshot]()

    var option = "createdAt"
    var personCommented = "Unknown User"

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        if topicObj != nil
        {
            setupTopicDetail()
            queryComment()
            
            DataService.instance.REF_TOPICS.child(topicObj.key).observe(.value, with: { (topicSnapshot) in
                
                self.topicObj = topicSnapshot
                
            })
        }
        
        
    }
    
    
    func setupTopicDetail()
    {
        if let topicData = topicObj.value as? Dictionary<String,Any>
        {
            let topicText = topicData["Text"] as! String
            topicLabel.text = topicText
            
            let votes = topicData["Votes"] as! Int
            topicVotesLabel.text = "\(votes)"
            
            let userPointer = topicData["TopicPointer"] as! String
            
            DataService.instance.REF_USERS.child(userPointer).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if let userData = snapshot.value as? Dictionary<String,Any>
                {
                    let username = userData["username"] as! String
                    let profileUrl = userData["UserProfileUrl"] as! String
                    
                    self.usernameLabel.text = username
                    self.avatarImg.sd_setImage(with: URL(string: "\(profileUrl)"), placeholderImage: UIImage(named: "logo"), options: [.continueInBackground, .progressiveDownload], completed: nil)
                    
                }
                
            })
            
        }
    }
    
    
    
    
    func queryComment()
    {
        showHUD("Please Wait ..")
        DataService.instance.REF_COMMENTS.child(topicObj.key).queryOrdered(byChild: option).observe(.value) { (snapshots) in

            self.commentArray.removeAll()
            if let commentSnaps = snapshots.children.allObjects as? [DataSnapshot]
            {
                for commentDict in commentSnaps
                {
                    if let comment = commentDict.value as? Dictionary<String,Any>
                    {
                        let reported = comment["Reported"] as! Bool
                        
                        if (!reported)
                        {
                            self.commentArray.insert(commentDict, at: 0)
                        }
                    }
                }
                self.hideHUD()
                self.tableView.reloadData()
                
            }

        }
    }


    
    @IBAction func backTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func writeCommentTapped(_ sender: Any) {
        
        if Auth.auth().currentUser != nil
        {
            if topicObj != nil
            {
                addComment()
            }
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func upvoteTapped(_ sender: Any)
    {
        upvoteTopic()
    }
    
    
    
    @IBAction func downvoteTapped(_ sender: Any)
    {
        downvoteTopic()
    }
    
    
    @IBAction func upVoteCommentButt(_ sender: Any) {
        if Auth.auth().currentUser != nil
        {
            let butt = sender as! UIButton
            upvoteComment(tagIndex: butt.tag)
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func downVoteCommentButt(_ sender: Any) {
        if Auth.auth().currentUser != nil
        {
            let butt = sender as! UIButton
            downvoteComment(tagIndex: butt.tag)
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func TopicavatarButt(_ sender: Any)
    {
        if topicObj != nil
        {
            if let topicData = topicObj.value as? Dictionary<String,Any>
            {
                let userPointer = topicData["TopicPointer"] as! String
                let aVC = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfile") as! UserProfile
                aVC.userObj = userPointer
                self.navigationController?.pushViewController(aVC, animated: true)
            }
        }
        
    }
    
    
    @IBAction func CommentavatarButt(_ sender: Any)
    {
        let butt = sender as! UIButton
        let commentObj = commentArray[butt.tag]
        
        if let commentData = commentObj.value as? Dictionary<String,Any>
        {
            let userPointer = commentData["UserPointer"] as! String
            let aVC = self.storyboard?.instantiateViewController(withIdentifier: "OtherUserProfile") as! UserProfile
            aVC.userObj = userPointer
            self.navigationController?.pushViewController(aVC, animated: true)
        }
        
        
    }
    
    
    @IBAction func latestCommentTapped(_ sender: Any) {
        option = "createdAt"
        queryComment()
        
    }
    
    @IBAction func bestCommentTapped(_ sender: Any) {
        option = "Votes"
        queryComment()
    }
    
    
    @IBAction func optionButt(_ sender: Any)
    {
        if Auth.auth().currentUser != nil
        {
            let butt = sender as! UIButton
            let commentObj = commentArray[butt.tag]
            
            let alert = UIAlertController(title: "Topics", message: "Select an option", preferredStyle: .actionSheet)
            
            
            let reportComment = UIAlertAction(title: "Report Inappropriate Comment", style: .default, handler: { (action) -> Void in
                
                self.showHUD("Please wait...")
                
                if (commentObj.value as? Dictionary<String,Any>) != nil
                {
                DataService.instance.REF_COMMENTS.child(self.topicObj.key).child(commentObj.key).updateChildValues(["Reported":true])
                DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                        if let userDict = snapshot.value as? Dictionary<String, AnyObject>
                        {
                            var userPoints = userDict["userPoints"] as! Int
                            userPoints += 5
                            let userKarma = ["userPoints": userPoints] as [String: Any]
                            DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).updateChildValues(userKarma)
                        }
                    self.hideHUD()
                    })
                }
                
            })
            
            
            let deleteComment = UIAlertAction(title: "Delete Comment", style: .destructive, handler: { (action) -> Void in
                
                self.showHUD("Please wait...")
                
                if let commentData = commentObj.value as? Dictionary<String,Any>
                {
                    let userPointer = commentData["UserPointer"] as! String
                    if userPointer != Auth.auth().currentUser!.uid
                    {
                        self.hideHUD()
                        self.showErrorAlert(_message: "You can't delete others comments")
                    }
                    else
                    {
                    DataService.instance.REF_COMMENTS.child(self.topicObj.key).child(commentObj.key).removeValue()
                        self.hideHUD()
                    }
                }
                
                
            })
            
            
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in })
            
            alert.addAction(reportComment)
            alert.addAction(deleteComment)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
            
        }
        
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
        
    }
    
    
    
    
    
    
    
    func upvoteComment(tagIndex: Int)
    {

        let currentUser = Auth.auth().currentUser?.uid
        
        let commentObj = commentArray[tagIndex]
        
        if let commentData = commentObj.value as? Dictionary<String,Any>
        {
            let userPointer = commentData["UserPointer"] as! String
            
            if userPointer != currentUser
            {
                if commentObj.hasChild("upVotedBy")
                {
                    let UpVotes = commentData["upVotedBy"] as! Dictionary<String,Bool>
                    if UpVotes.keys.contains(currentUser!)
                    {
                        self.showErrorAlert(_message: "You've already upvoted this Topic!")
                    }
                    else
                    {
                    DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).child("upVotedBy").updateChildValues([currentUser!:true])
                        
                        self.incrementCommentVote(index: tagIndex)
                        
                        if commentObj.hasChild("downVotedBy")
                        {
                            let downVotes = commentData["downVotedBy"] as! Dictionary<String,Bool>
                            if downVotes.keys.contains(currentUser!)
                            {
                            DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).child("downVotedBy").child(currentUser!).removeValue()
                            }
                        }
                    }
                }
                else
                {
                DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).child("upVotedBy").updateChildValues([currentUser!:true])
                    
                    self.incrementCommentVote(index: tagIndex)
                    
                    if commentObj.hasChild("downVotedBy")
                    {
                        let downVotes = commentData["downVotedBy"] as! Dictionary<String,Bool>
                        if downVotes.keys.contains(currentUser!)
                        {
                            DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).child("downVotedBy").child(currentUser!).removeValue()
                        }
                    }
                }
            }
            else
            {
                self.showErrorAlert(_message: "You can't upvote your own topic")
            }
        }
        
    }
    
    func downvoteComment(tagIndex: Int)
    {
        let currentUser = Auth.auth().currentUser?.uid
        
        let commentObj = commentArray[tagIndex]
        
        if let commentData = commentObj.value as? Dictionary<String,Any>
        {
            let userPointer = commentData["UserPointer"] as! String
            
            if userPointer != currentUser
            {
                if commentObj.hasChild("downVotedBy")
                {
                    let UpVotes = commentData["downVotedBy"] as! Dictionary<String,Bool>
                    if UpVotes.keys.contains(currentUser!)
                    {
                        self.showErrorAlert(_message: "You've already downvoted this Topic!")
                    }
                    else
                    {
                    DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).child("downVotedBy").updateChildValues([currentUser!:true])
                        
                        self.decrementCommentVote(index: tagIndex)
                        
                        if commentObj.hasChild("upVotedBy")
                        {
                            let downVotes = commentData["upVotedBy"] as! Dictionary<String,Bool>
                            if downVotes.keys.contains(currentUser!)
                            {
                                DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).child("upVotedBy").child(currentUser!).removeValue()
                            }
                        }
                    }
                }
                else
                {
                DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).child("downVotedBy").updateChildValues([currentUser!:true])
                    
                    self.decrementCommentVote(index: tagIndex)
                    
                    if commentObj.hasChild("upVotedBy")
                    {
                        let downVotes = commentData["upVotedBy"] as! Dictionary<String,Bool>
                        if downVotes.keys.contains(currentUser!)
                        {
                            DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).child("upVotedBy").child(currentUser!).removeValue()
                        }
                    }
                }
            }
            else
            {
                self.showErrorAlert(_message: "You can't downvote your own topic")
            }
        }
        
            
    }
    
    
    
    
    func incrementCommentVote(index: Int)
    {
        let indexP = IndexPath(row: index, section: 0)
        let cell = tableView.cellForRow(at: indexP) as! CommentCell
        let commentObj = commentArray[index]
        if let commentData = commentObj.value as? Dictionary<String,Any>
        {
            if var votes = commentData["Votes"] as? Int
            {
                votes = votes + 1
                cell.votesLabel.text = votes.abbreviated
            DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).updateChildValues(["Votes": votes])
            DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if let userDict = snapshot.value as? Dictionary<String, AnyObject>
                    {
                        var userPoints = userDict["userPoints"] as! Int
                        let username = userDict["username"] as! String
                        self.personCommented = username.capitalized
                        userPoints += 5
                        let userKarma = ["userPoints": userPoints] as [String: Any]
                        DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).updateChildValues(userKarma)
                        self.sendNotisficationforComment("upvoted your comment", index: index)
                        
                    }
                })
            }
        }
        
    }
    
    
    func decrementCommentVote(index: Int)
    {
        let indexP = IndexPath(row: index, section: 0)
        let cell = tableView.cellForRow(at: indexP) as! CommentCell
        let commentObj = commentArray[index]
        if let commentData = commentObj.value as? Dictionary<String,Any>
        {
            if var votes = commentData["Votes"] as? Int
            {
                votes = votes - 1
                cell.votesLabel.text = votes.abbreviated
                DataService.instance.REF_COMMENTS.child(topicObj.key).child(commentObj.key).updateChildValues(["Votes": votes])
                DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if let userDict = snapshot.value as? Dictionary<String, AnyObject>
                    {
                        let username = userDict["username"] as! String
                        self.personCommented = username.capitalized
                        self.sendNotisficationforComment("downvoted your comment", index: index)
                        
                    }
                })
            }
        }
    }
    
    
    
    
    func upvoteTopic()
    {
        if Auth.auth().currentUser != nil
        {
            let currentUser = Auth.auth().currentUser?.uid
            
            if let topicData = topicObj.value as? Dictionary<String,Any>
            {
                let userPointer = topicData["TopicPointer"] as! String
                
                if userPointer != currentUser
                {
                    if topicObj.hasChild("upVotedBy")
                    {
                        let UpVotes = topicData["upVotedBy"] as! Dictionary<String,Bool>
                        if UpVotes.keys.contains(currentUser!)
                        {
                            self.showErrorAlert(_message: "You've already upvoted this Topic!")
                        }
                        else
                        {
                        DataService.instance.REF_TOPICS.child(topicObj.key).child("upVotedBy").updateChildValues([currentUser!:true])
                            self.incrementVote()
                            
                            if topicObj.hasChild("downVotedBy")
                            {
                                let downVotes = topicData["downVotedBy"] as! Dictionary<String,Bool>
                                if downVotes.keys.contains(currentUser!)
                                {
                                DataService.instance.REF_TOPICS.child(topicObj.key).child("downVotedBy").child(currentUser!).removeValue()
                                }
                            }
                        }
                    }
                    else
                    {
                    DataService.instance.REF_TOPICS.child(topicObj.key).child("upVotedBy").updateChildValues([currentUser!:true])
                        self.incrementVote()
                        
                        if topicObj.hasChild("downVotedBy")
                        {
                            let downVotes = topicData["downVotedBy"] as! Dictionary<String,Bool>
                            if downVotes.keys.contains(currentUser!)
                            {
                            DataService.instance.REF_TOPICS.child(topicObj.key).child("downVotedBy").child(currentUser!).removeValue()
                            }
                        }
                    }
                }
                else
                {
                    self.showErrorAlert(_message: "You can't upvote your own topic")
                }
            }
            
            
            
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
    }
    
    func downvoteTopic()
    {
        if Auth.auth().currentUser != nil
        {
            let currentUser = Auth.auth().currentUser?.uid
            
            if let topicData = topicObj.value as? Dictionary<String,Any>
            {
                let userPointer = topicData["TopicPointer"] as! String
                
                if userPointer != currentUser
                {
                    if topicObj.hasChild("downVotedBy")
                    {
                        let downVotes = topicData["downVotedBy"] as! Dictionary<String,Bool>
                        if downVotes.keys.contains(currentUser!)
                        {
                            self.showErrorAlert(_message: "You've already downvoted this Topic!")
                        }
                        else
                        {
                        DataService.instance.REF_TOPICS.child(topicObj.key).child("downVotedBy").updateChildValues([currentUser!:true])
                            self.decrementVote()
                            
                            
                            if topicObj.hasChild("upVotedBy")
                            {
                                let upVotes = topicData["upVotedBy"] as! Dictionary<String,Bool>
                                if upVotes.keys.contains(currentUser!)
                                {
                                    DataService.instance.REF_TOPICS.child(topicObj.key).child("upVotedBy").child(currentUser!).removeValue()
                                }
                            }
                        }
                        
                    }
                    else
                    {
                    DataService.instance.REF_TOPICS.child(topicObj.key).child("downVotedBy").updateChildValues([currentUser!:true])
                        self.decrementVote()
                        
                        
                        if topicObj.hasChild("upVotedBy")
                        {
                            let upVotes = topicData["upVotedBy"] as! Dictionary<String,Bool>
                            if upVotes.keys.contains(currentUser!)
                            {
                                DataService.instance.REF_TOPICS.child(topicObj.key).child("upVotedBy").child(currentUser!).removeValue()
                            }
                        }
                        
                    }
                }
                else
                {
                    self.showErrorAlert(_message: "You can't downvote your own topic")
                }
                
            }
            
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
        
    }
    
    
    
    
    func incrementVote()
    {
        if topicObj != nil
        {
            if let topicData = topicObj.value as? Dictionary<String,Any>
            {
                if var votes = topicData["Votes"] as? Int
                {
                    votes = votes + 1
                    topicVotesLabel.text = "\(votes)"
                    DataService.instance.REF_TOPICS.child(topicObj.key).updateChildValues(["Votes": votes])
                DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        if let userDict = snapshot.value as? Dictionary<String, AnyObject>
                        {
                            var userPoints = userDict["userPoints"] as! Int
                            let username = userDict["username"] as! String
                            self.personCommented = username.capitalized
                            userPoints += 5
                            let userKarma = ["userPoints": userPoints] as [String: Any]
                        DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).updateChildValues(userKarma)
                            self.sendNotisficationforTopic("upvoted your topic")
                            
                        }
                    })
                }
            }
        }
    }
    
    func decrementVote()
    {
        if topicObj != nil
        {
            if let topicData = topicObj.value as? Dictionary<String,Any>
            {
                if var votes = topicData["Votes"] as? Int
                {
                    votes = votes - 1
                    topicVotesLabel.text = "\(votes)"
                    DataService.instance.REF_TOPICS.child(topicObj.key).updateChildValues(["Votes": votes])
                    DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        if let userDict = snapshot.value as? Dictionary<String, AnyObject>
                        {
                            let username = userDict["username"] as! String
                            self.personCommented = username.capitalized
                            self.sendNotisficationforTopic("downvoted your topic")
                        }
                    })
                }
            }
        }
    }
    
    
    
    
    
    func addComment()
    {
        if Auth.auth().currentUser != nil
        {
            let alert = UIAlertController(title: "Topics",
                                          message: "Write a comment",
                                          preferredStyle: .alert)
            
            
            let ok = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
                
                let textField = alert.textFields!.first!
                let txtStr = textField.text!
                
                if txtStr != ""
                {
                    let commentData = ["Text":txtStr,"UserPointer":Auth.auth().currentUser!.uid,"Votes":0,"Reported":false,"createdAt": ServerValue.timestamp()] as [String:Any]
                    
                    
                    DataService.instance.REF_TOPICS.child(self.topicObj.key).observeSingleEvent(of: .value, with: { (commentSnap) in
                        
                        if let commentDict = commentSnap.value as? Dictionary<String, AnyObject>
                        {
                            var comments = commentDict["Comments"] as! Int
                            comments += 1
                            
                            let topicComment = ["Comments": comments] as [String: Any]
                        DataService.instance.REF_TOPICS.child(self.topicObj.key).updateChildValues(topicComment)
                        }
                        
                    })
                    DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        if let userDict = snapshot.value as? Dictionary<String, AnyObject>
                        {
                            var userPoints = userDict["userPoints"] as! Int
                            let username = userDict["username"] as! String
                            self.personCommented = username.capitalized
                            userPoints += 5
                            let userKarma = ["userPoints": userPoints] as [String: Any]
                            
                            DataService.instance.createTopicComment(uid: Auth.auth().currentUser!.uid, commentData: commentData, userData: userKarma, topicId: self.topicObj.key)
                            self.sendNotisficationforTopic("commented on your topic")
                        }
                    })
                    
                }else
                {
                    self.showErrorAlert(_message: "Please type something!")
                    
                }
            })
            
            
            // Cancel button
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
            
            // Add textField
            alert.addTextField { (textField: UITextField) in
                textField.keyboardAppearance = .dark
                textField.keyboardType = .default
                textField.font = UIFont(name: "Titillium-Regular", size: 12)
                textField.autocorrectionType = .default
            }
            
            alert.addAction(ok)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
    }
    
    
    
    func sendNotisficationforTopic(_ message: String)
    {
        if topicObj != nil
        {
            if let user = topicObj.value as? Dictionary<String,Any>
            {
                if let userPointer = user["TopicPointer"] as? String, userPointer != Auth.auth().currentUser?.uid
                {
                    DataService.instance.REF_USERS.child(userPointer).observeSingleEvent(of: .value, with: { (snapshots) in
                        if let userSnapsot = snapshots.value as? Dictionary<String,Any>
                        {
                            if let playerID = userSnapsot["playerID"] as? String
                            {
                                OneSignal.postNotification(["contents": ["en": "\(self.personCommented) \(message)"], "include_player_ids": [playerID]])

                            }
                        }
                    })
                }
            }
        }
        
    }
    
    
    func sendNotisficationforComment(_ message: String, index : Int)
    {
        if let commentDict = commentArray[index].value as? Dictionary<String,Any>
        {
            let commentObject = commentDict["UserPointer"] as! String
            
            if commentObject != Auth.auth().currentUser?.uid
            {
                DataService.instance.REF_USERS.child(commentObject).observeSingleEvent(of: .value, with: { (snapshots) in
                    if let userSnapsot = snapshots.value as? Dictionary<String,Any>
                    {
                        if let playerID = userSnapsot["playerID"] as? String
                        {
                            OneSignal.postNotification(["contents": ["en": "\(self.personCommented) \(message)"], "include_player_ids": [playerID]])
                            
                        }
                    }
                })
            }
        }
        
        
    }
    
    
    
    
    
}


extension CommentVC : UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
        cell.upButton.tag = indexPath.row
        cell.downButton.tag = indexPath.row
        cell.configureCell(comment: commentArray[indexPath.row])
        return cell
    }
    
    
    
}

