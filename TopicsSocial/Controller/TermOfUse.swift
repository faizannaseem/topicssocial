//
//  TermOfUse.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

class TermOfUse: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = Bundle.main.url(forResource: "tou", withExtension: "html")
        webView.loadRequest(URLRequest(url: url!))
        
    }
   

    @IBAction func dismisssTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    

}
