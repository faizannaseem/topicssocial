//
//  EditProfile.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import FirebaseStorage

class EditProfile: UIViewController, UITextFieldDelegate, Alertable {

    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var usernameTxt: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        showUserDetails()
    }
    
    
    func showUserDetails()
    {
        DataService.instance.REF_USERS.child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value) { (snapshot) in
            
            if let userSnap = snapshot.value as? Dictionary<String,Any>
            {
                let username = userSnap["username"] as! String
                let profileUrl = userSnap["UserProfileUrl"] as! String
                
                self.avatarImg.sd_setImage(with: URL(string: "\(profileUrl)"), placeholderImage: UIImage(named: "user_avatar"), options: [.continueInBackground, .progressiveDownload], completed: nil)
                self.usernameTxt.text = username
            }
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    @IBAction func updateProfileButt(_ sender: Any) {
        
        showHUD("Please Wait ..")
        let currUser = Auth.auth().currentUser!.uid
        
        if usernameTxt.text != "" {
            //print("Please wait...")
            dismissKeyboard()
           
            PhotoUploadUrl(completion: { (url) in
                
                let userData = ["username": self.usernameTxt.text!, "UserProfileUrl": url] as [String:Any]
                DataService.instance.REF_USERS.child(currUser).updateChildValues(userData)
                self.hideHUD()
                self.simpleAlert("Your Profile has been updated!")
                _ = self.navigationController?.popViewController(animated: true)
            })
         
        } else { showErrorAlert(_message: "You must type a username and an email address!") }
    }

    
    
    
    @IBAction func changeAvatarButt(_ sender: Any)
    {
        let alert = UIAlertController(title: "Topics",
                                      message: "Select source",
                                      preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Take a picture", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let library = UIAlertAction(title: "Pick from Library", style: .default, handler: { (action) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(camera)
        alert.addAction(library)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
    }
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
 
    func dismissKeyboard() {
        usernameTxt.resignFirstResponder()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func PhotoUploadUrl(completion: @escaping (String)->())
    {
        if avatarImg != nil
        {
            let data = UIImageJPEGRepresentation(avatarImg.image!, 0.8)
            let filePath = "\(Auth.auth().currentUser!.uid)/\("defaultImage")"
            Storage.storage().reference().child(filePath).putData(data!, metadata: nil) { (metaData, error) in
                guard let metaData = metaData else {
                    print(error ?? "")
                    return
                    
                }
                
                let downloadUrl  = metaData.downloadURL()?.absoluteString
                completion(downloadUrl!)
            }
            
        }
        
    }
    
}



extension EditProfile: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            avatarImg.image = image
        }
        dismiss(animated: true, completion: nil)
    }
}





