//
//  LoginVC.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import OneSignal

class LoginVC: UIViewController, Alertable {

    @IBOutlet weak var pwdField: DesginableField!
    @IBOutlet weak var emailField: DesginableField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if Auth.auth().currentUser != nil
        {
             dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func signInTapped(_sender: Any)
    {
        showHUD("Please Wait ..")
        
        guard let email = emailField.text, let password = pwdField.text, email != "" , password != "" else {
            
            self.hideHUD()
            self.showErrorAlert(_message: "Please fill all fields")
            return
            
        }
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if error == nil
            {
                self.hideHUD()
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.hideHUD()
                self.showErrorAlert(_message: error!.localizedDescription)
                return
            }
            
        }
    }

    @IBAction func fbBtnTapped(_ sender: Any)
    {
        showHUD("Please Wait ..")
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) in
            if let error = error {
                self.hideHUD()
                self.showErrorAlert(_message: error.localizedDescription)
                return
            }
            
            guard let accessToken = FBSDKAccessToken.current() else {
                self.hideHUD()
                self.showErrorAlert(_message: "Failed to get access token")
                return
            }
            
            let credential = FacebookAuthProvider.credential(withAccessToken: accessToken.tokenString)
            
            Auth.auth().signIn(with: credential, completion: { (user, error) in
                if let error = error {
                    self.hideHUD()
                    self.showErrorAlert(_message: error.localizedDescription)
                    return
                }
                self.hideHUD()
                self.dismiss(animated: true, completion: nil)
                self.getFBUserData()
                
            })
            
        }
    }
    
    
    @IBAction func signUpTapped(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        present(aVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func forgotPassTapped(_ sender: Any)
    {
        showHUD("Please Wait ..")
        let alertController = UIAlertController(title: "Forgot Password", message: "Enter your email address to recover password", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Ok", style: .default, handler: {
            alert -> Void in
            
            let emailTextField = alertController.textFields![0] as UITextField
            
            
            if emailTextField.text != ""{
                
                Auth.auth().sendPasswordReset(withEmail: emailTextField.text!, completion: { (error) in
                    
                    if let error = error
                    {
                        self.hideHUD()
                        self.showErrorAlert(_message: error.localizedDescription)
                        return
                    }
                    
                    self.hideHUD()
                    self.simpleAlert("Your Password recovery email has been sent. Check your email to recover it.")
                    
                })
                
            }
            else{
                self.hideHUD()
                self.showErrorAlert(_message: "Field should not be empty, Please enter email address..")
                return
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.hideHUD()
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Email"
        }
        
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    
    func getFBUserData()
    {
        let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large)"])
        let _ = request?.start(completionHandler: { (connection, result, error) in
            guard let userInfo = result as? [String: Any] else { return }
            
            if let imageURL = ((userInfo["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String {
                
                let username = userInfo["name"] as! String
                let email = userInfo["email"] as! String
                
                let status : OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
                let userID = status.subscriptionStatus.userId
                
                var playerID = ""
                
                if userID != nil
                {
                    playerID = userID!
                }
                
                let userData = ["userEmail":email, "username": username, "provider": "Facebook", "userReported":false, "userPoints":0, "UserProfileUrl": imageURL, "playerID": playerID,"createdAt": ServerValue.timestamp() ] as [String:Any]
                AuthService.instance.createFirebaseUser(uid: Auth.auth().currentUser!.uid, userData: userData)
                
            }
        })
    
    }
    
    
}
