//
//  UserProfile.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase

class UserProfile: UIViewController, Alertable {

    var userObj : String!
    var isTopics = true
    var topicsCommentsArray = [DataSnapshot]()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var karmaLabel: UILabel!
    
    @IBOutlet weak var topicsOutlet: UIButton!
    @IBOutlet weak var commentsOutlet: UIButton!
    @IBOutlet weak var aboutOutlet: UIButton!
    
    @IBOutlet weak var aboutKarmaLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var otherUserTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getUserDetails()
        queryTopics()
        
    }
    
    
    func getUserDetails()
    {
        if userObj != nil
        {
            DataService.instance.REF_USERS.child(userObj).observeSingleEvent(of: .value, with: { (userSnapshots) in
                
                if let userData = userSnapshots.value as? Dictionary<String,Any>
                {
                    let username = userData["username"] as! String
                    self.titleLabel.text = username
                    
                    let userPoints = userData["userPoints"] as! Int
                    self.karmaLabel.text = "\(userPoints) karma"
                    
                    let profileUrl = userData["UserProfileUrl"] as! String
                    
                    self.avatarImg.sd_setImage(with: URL(string: "\(profileUrl)"), placeholderImage: UIImage(named: "logo"), options: [.continueInBackground, .progressiveDownload], completed: nil)
                    
                }
                
                
            })
        }
    }
    
    
    @IBAction func topicsButt(_ sender: Any) {
        queryTopics()
    }
    
    @IBAction func commentsButt(_ sender: Any) {
        queryComments()
    }

    @IBAction func backTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    @IBAction func reportUserButt(_ sender: Any)
    {
        let alert = UIAlertController(title: "Topics",
                                      message: "Select option",
                                      preferredStyle: .alert)
        
        
        let reportUser = UIAlertAction(title: "Report Offensive User", style: .default, handler: { (action) -> Void in
            
            DataService.instance.REF_USERS.child(self.userObj).updateChildValues(["userReported":true])
                self.showSuccessAlert(_message: "Thanks for reporting this User, we'll check it out withint 24 hours!")
            
        })
        
        // Cancel button
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(reportUser)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        
        
        
    }
    
    
    
    
    func queryTopics()
    {
        showHUD("Please Wait ..")
        
        topicsOutlet.setTitleColor(MAIN_COLOR, for: .normal)
        commentsOutlet.setTitleColor(UIColor.darkGray, for: .normal)
        aboutOutlet.setTitleColor(UIColor.darkGray, for: .normal)
        
        isTopics = true
        topicsCommentsArray.removeAll()
        otherUserTableView.reloadData()
        
        DataService.instance.REF_TOPICS.observeSingleEvent(of: .value) { (snapshots) in
            
            if let topicSnaps = snapshots.children.allObjects as? [DataSnapshot]
            {
                for topicDict in topicSnaps
                {
                    if let topic =  topicDict.value as? Dictionary<String,Any>
                    {
                        let TopicPointer = topic["TopicPointer"] as? String
                        let reported = topic["TopicReported"] as! Bool
                        
                        if (!reported)
                        {
                            if TopicPointer == self.userObj
                            {
                                self.topicsCommentsArray.append(topicDict)
                            }
                        }
                    }
                }
                self.hideHUD()
                self.otherUserTableView.reloadData()
            }
            
        }

    }
    
    
    func queryComments()
    {
        showHUD("Please Wait ..")
        
        topicsOutlet.setTitleColor(UIColor.darkGray, for: .normal)
        commentsOutlet.setTitleColor(MAIN_COLOR, for: .normal)
        aboutOutlet.setTitleColor(UIColor.darkGray, for: .normal)
        
        
        isTopics = false
        topicsCommentsArray.removeAll()
        otherUserTableView.reloadData()
        
        
        DataService.instance.REF_COMMENTS.observeSingleEvent(of: .value) { (snapshots) in
            
            if let commentSnaps = snapshots.children.allObjects as? [DataSnapshot]
            {
                for commentDict in commentSnaps
                {
                    if let comments = commentDict.children.allObjects as? [DataSnapshot]
                    {
                        for comment in comments
                        {
                            if let commentData = comment.value as? Dictionary<String,Any>
                            {
                                let commentPointer = commentData["UserPointer"] as? String
                                let reported = commentData["Reported"] as! Bool
                                
                                if (!reported)
                                {
                                    if commentPointer == self.userObj
                                    {
                                        self.topicsCommentsArray.append(commentDict)
                                    }
                                }
                            }
                        }
                    }
                }
                self.hideHUD()
                self.otherUserTableView.reloadData()
            }
            
        }
   
    }
    
    
    

}


extension UserProfile : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topicsCommentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userCell = tableView.dequeueReusableCell(withIdentifier: "OtherUserCell", for: indexPath) as! OtherUserCell
        
        if isTopics {
            if let activity = topicsCommentsArray[indexPath.row].value as? Dictionary<String,Any>
            {
                let text = activity["Text"] as! String
                userCell.topicTitleLabel.text = text
                let comment = activity["Comments"] as! Int
                userCell.commentLabel.text = "\(comment) Comments"
                
            }
        }
        else{
            
            if let activities = topicsCommentsArray[indexPath.row].value as? Dictionary<String,Any>
            {
                for activity in activities
                {
                    if let commentData = activity.value as? Dictionary<String,Any>
                    {
                        let text = commentData["Text"] as! String
                        userCell.commentLabel.text = text
                        DataService.instance.REF_TOPICS.child(topicsCommentsArray[indexPath.row].key).observeSingleEvent(of: .value, with: { (snapshots) in
                            
                            if let topicData = snapshots.value as? Dictionary<String,Any>
                            {
                                let topicText = topicData["Text"] as! String
                                userCell.topicTitleLabel.text = topicText
                            }
                            
                        })
                    }
                }
                
                
                
            }
            
            
            
        }
        
        
        
        
        return userCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isTopics{
            let aVC = storyboard?.instantiateViewController(withIdentifier: "CommentVC") as! CommentVC
            aVC.topicObj = topicsCommentsArray[indexPath.row]
            navigationController?.pushViewController(aVC, animated: true)
        }
        
        
    }
    
    
    
    
}
















