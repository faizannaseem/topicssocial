//
//  AccountVC.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
// -L3NL97QQ9hVD9KzAYsL

import UIKit
import Firebase
import SDWebImage

class AccountVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var karmaLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var topicsOutlet: UIButton!
    @IBOutlet weak var commentsOutlet: UIButton!
    @IBOutlet weak var activityOutlet: UIButton!
    
    var topicsCommentsActivityArray = [DataSnapshot]()
    var isTopics = true
    var isActivity = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        queryTopics()
     
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        showUserDetails()
    }
    
    
    
    
    @IBAction func signOutTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Topics",
                                      message: "Are you sure you want to logout?",
                                      preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Logout", style: .default, handler: { (action) -> Void in
            print("Logging Out...")
            
            do {
                try Auth.auth().signOut()
                _ = self.navigationController?.popToRootViewController(animated: true)
            }catch(let error)
            {
                print(error)
            }
           
        })
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in })
        
        alert.addAction(ok); alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }

    @IBAction func backTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func topicsButt(_ sender: Any) {
        queryTopics()
    }
    
    @IBAction func commentsButt(_ sender: Any) {
       queryComments()
    }

    @IBAction func activityButt(_ sender: Any) {
      //  queryActivity()
    }
    
    @IBAction func editProfileButt(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
        navigationController?.pushViewController(aVC, animated: true)
    }
    
    
    func showUserDetails() {
        let currUser = Auth.auth().currentUser?.uid
        
        if currUser != nil
        {
            DataService.instance.REF_USERS.child(currUser!).observeSingleEvent(of: .value, with: { (usersnaps) in
                
                if let userData = usersnaps.value as? Dictionary<String,Any>
                {
                    let username = userData["username"] as! String
                    self.usernameLabel.text = username
                    
                    let userPoints = userData["userPoints"] as! Int
                    self.karmaLabel.text = "\(userPoints)"
                    
                    let profileUrl = userData["UserProfileUrl"] as! String
                    
                    self.avatarImg.sd_setImage(with: URL(string: "\(profileUrl)"), placeholderImage: UIImage(named: "logo"), options: [.continueInBackground, .progressiveDownload], completed: nil)
                    
                    
                    let timestamp = userData["createdAt"] as! TimeInterval
                    let date = Date(timeIntervalSince1970: timestamp / 1000)
                    
                    let now = Date()
                    self.ageLabel.text = self.timeAgoSinceDate(date, currentDate: now, numericDates: true)
                    
                }
                
            })
            
            
        }
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    
    func queryTopics()
    {
        showHUD("Please Wait ..")
        let currUser = Auth.auth().currentUser!.uid
        
        topicsOutlet.setTitleColor(MAIN_COLOR, for: .normal)
        commentsOutlet.setTitleColor(UIColor.darkGray, for: .normal)
        activityOutlet.setTitleColor(UIColor.darkGray, for: .normal)
        
        isTopics = true
        isActivity = false
        topicsCommentsActivityArray.removeAll()
        tableView.reloadData()
        
        
        DataService.instance.REF_TOPICS.observe(.value) { (snapshots) in
            if let topicSnaps = snapshots.children.allObjects as? [DataSnapshot]
            {
                self.topicsCommentsActivityArray = []
                for topicDict in topicSnaps
                {
                    if let topic =  topicDict.value as? Dictionary<String,Any>
                    {
                        let TopicPointer = topic["TopicPointer"] as? String
                        let reported = topic["TopicReported"] as! Bool
                        
                        if (!reported)
                        {
                            if TopicPointer == currUser
                            {
                                self.topicsCommentsActivityArray.append(topicDict)
                            }
                        }
                    }
                    
                }
                self.hideHUD()
                self.tableView.reloadData()
            }
        }
        
        
    }
    
    
    func queryComments()
    {
        showHUD("Please Wait ..")
        let currUser = Auth.auth().currentUser!.uid
        
        topicsOutlet.setTitleColor(UIColor.darkGray, for: .normal)
        commentsOutlet.setTitleColor(MAIN_COLOR, for: .normal)
        activityOutlet.setTitleColor(UIColor.darkGray, for: .normal)
        
        isTopics = false
        isActivity = false
        topicsCommentsActivityArray.removeAll()
        tableView.reloadData()
        
        DataService.instance.REF_COMMENTS.observeSingleEvent(of: .value) { (snapshots) in
            
            if let commentSnaps = snapshots.children.allObjects as? [DataSnapshot]
            {
                for commentDict in commentSnaps
                {
                    if let comments = commentDict.children.allObjects as? [DataSnapshot]
                    {
                        for comment in comments
                        {
                            if let commentData = comment.value as? Dictionary<String,Any>
                            {
                                let commentPointer = commentData["UserPointer"] as? String
                                let reported = commentData["Reported"] as! Bool
                                
                                if (!reported)
                                {
                                    if commentPointer == currUser
                                    {
                                        self.topicsCommentsActivityArray.append(commentDict)
                                    }
                                }
                            }
                        }
                    }
                }
                self.hideHUD()
                self.tableView.reloadData()
            }
            
        }
        
        
    }
    

}


extension AccountVC : UITableViewDataSource,UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topicsCommentsActivityArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userCell = tableView.dequeueReusableCell(withIdentifier: "OtherUserCell", for: indexPath) as! UserActivityCell
        
        if isTopics && !isActivity {
            if let activity = topicsCommentsActivityArray[indexPath.row].value as? Dictionary<String,Any>
            {
                let text = activity["Text"] as! String
                userCell.topicTitleLabel.text = text
                let comment = activity["Comments"] as! Int
                userCell.commentLabel.text = "\(comment) Comments"
                
            }
        }
        
        else if !isTopics && !isActivity {
            
            
            if let activities = topicsCommentsActivityArray[indexPath.row].value as? Dictionary<String,Any>
            {
                for activity in activities
                {
                    if let commentData = activity.value as? Dictionary<String,Any>
                    {
                            let text = commentData["Text"] as! String
                            userCell.commentLabel.text = text
                        DataService.instance.REF_TOPICS.child(topicsCommentsActivityArray[indexPath.row].key).observeSingleEvent(of: .value, with: { (snapshots) in
                            
                            if let topicData = snapshots.value as? Dictionary<String,Any>
                            {
                                let topicText = topicData["Text"] as! String
                                userCell.topicTitleLabel.text = topicText
                            }
                            
                        })
                    }
                }
                
            }
        }
        
        return userCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isTopics && !isActivity {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "CommentVC") as! CommentVC
            aVC.topicObj = topicsCommentsActivityArray[indexPath.row]
            navigationController?.pushViewController(aVC, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if isTopics && !isActivity {
            if editingStyle == .delete {
                let topic = topicsCommentsActivityArray[indexPath.row].key
                
                DataService.instance.REF_TOPICS.child(topic).removeValue()
                DataService.instance.REF_COMMENTS.child(topic).removeValue()
            }
        }
        
    }
    
    
}










