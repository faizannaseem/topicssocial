//
//  SearchVC.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class SearchVC: UIViewController,Alertable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var topicsArray = [DataSnapshot]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self

    }

    @IBAction func backTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func queryTopics(text: String)
    {
        showHUD("Please Wait ..")
        
        let typeKeywords = text.lowercased().components(separatedBy: " ")
        topicsArray = []
        
        DataService.instance.REF_TOPICS.observeSingleEvent(of: .value) { (snapshots) in
            
            if let topicSnapshot = snapshots.children.allObjects as? [DataSnapshot]
            {
                for topic in topicSnapshot
                {
                    if let topicsDict = topic.value as? Dictionary<String,Any>
                    {
                        let keywords = topicsDict["Keywords"] as! NSArray
                        let reported = topicsDict["TopicReported"] as! Bool
                        
                        if (!reported)
                        {
                            for typeKeyword in typeKeywords
                            {
                                if keywords.contains(typeKeyword)
                                {
                                    self.topicsArray.append(topic)
                                }
                            }
                        }
                        
                    }
                }
                self.hideHUD()
                self.tableView.reloadData()
            }
        }
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    

}


extension SearchVC : UITableViewDataSource, UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return topicsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SearchCell
        if topicsArray.count != 0
        {
            if let topic = topicsArray[indexPath.row].value as? Dictionary<String,Any>
            {
                let text = topic["Text"] as! String
                let comment = topic["Comments"] as! Int
                let topicPointer = topic["TopicPointer"] as! String
                
                
                DataService.instance.REF_USERS.child(topicPointer).observeSingleEvent(of: .value, with: { (snapshots) in
                    if let userSnapshot = snapshots.value as? Dictionary<String, Any>
                    {
                        if let username = userSnapshot["username"] as? String, let profileUrl = userSnapshot["UserProfileUrl"] as? String
                        {
                            cell.topicLabel.text = text
                            cell.commentslabel.text = "\(comment) comments • " + "\(username)"
                            cell.avatarImg.sd_setImage(with: URL(string: "\(profileUrl)"), placeholderImage: UIImage(named: "user_avatar"), options: [.continueInBackground, .progressiveDownload], completed: nil)
                        }
                        
                    }
                })
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "CommentVC") as! CommentVC
        aVC.topicObj = topicsArray[indexPath.row]
        navigationController?.pushViewController(aVC, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    
}


extension SearchVC: UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text != "" {
            queryTopics(text: searchBar.text!)
            searchBar.resignFirstResponder()
            
        } else { showErrorAlert(_message: "You must type somehting!") }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = ""
        searchBar.showsCancelButton = false
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        searchBar.resignFirstResponder()
    }
}












