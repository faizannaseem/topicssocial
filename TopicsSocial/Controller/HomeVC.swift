//
//  HomeVC.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase

class HomeVC: UIViewController, Alertable {
    
    
    @IBOutlet weak var topicLbl: UILabel!
    @IBOutlet weak var commentsOutlet: UIButton!
    @IBOutlet weak var topicView: UIView!
    
    
    var currentUser : String!
    var topicNumber = 0
    var topicsArray =  [DataSnapshot]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        currentUser = Auth.auth().currentUser?.uid
        commentsOutlet.isEnabled  = false
        queryTopics()
        
         
    }

    @IBAction func searchTapped(_ sender: Any) {
        
        let aVC = storyboard?.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        navigationController?.pushViewController(aVC, animated: true)
        
    }
    
    @IBAction func shareTapped(_ sender: Any) {
    
        if topicsArray.count != 0 {
            
            if let topic = topicsArray[topicNumber].value as? Dictionary<String,Any>
            {
                if let text = topic["Text"] as? String
                {
                    let messageStr  = "\(text) | on #Topics"
                    let img = UIImage(named: "logo")!
                    
                    let shareItems = [messageStr, img] as [Any]
                    
                    let actVC = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
                    actVC.excludedActivityTypes = [.print, .postToWeibo, .copyToPasteboard, .addToReadingList, .postToVimeo]
                    present(actVC, animated: true, completion: nil)

                }
                
            }
            
        }
    }
    
    @IBAction func reportUserTapped(_ sender: Any) {
        if Auth.auth().currentUser != nil
        {
            let alert = UIAlertController(title: "Topics",message: "Report Topic", preferredStyle: .actionSheet)
            
            let ok = UIAlertAction(title: "Inappropriate/Offensive", style: .default, handler: { (action) -> Void in
                
                
                if self.topicsArray.count > 0
                {
                DataService.instance.REF_TOPICS.child(self.topicsArray[self.topicNumber].key).updateChildValues(["TopicReported":true])
                    
                    self.showSuccessAlert(_message: "Thanks for reporting this Topic. We'll check it out within 24h.")
                }
                
            })
            
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
            alert.addAction(ok)
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
            
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func addTopicTapped(_ sender: Any) {
        
        if Auth.auth().currentUser != nil
        {
            addTopic()
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func randomTopic(_ sender: Any) {
        if topicsArray.count != 0 {
            topicNumber = Int(arc4random() % UInt32(topicsArray.count - 1))
            print("RANDOM TOPIC NUMBER: \(topicNumber)")
            
            showTopics()
        }
    }
    
    @IBAction func accountTapped(_ sender: Any) {
        
        if Auth.auth().currentUser != nil
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "AccountVC") as! AccountVC
            navigationController?.pushViewController(aVC, animated: true)
        }
        else
        {
            let aVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            present(aVC, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func readRulesTapped(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "TermOfUse") as! TermOfUse
        present(aVC, animated: true, completion: nil)
    }
    
    
    @IBAction func commentOutletTapped(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "CommentVC") as! CommentVC
        aVC.topicObj = topicsArray[topicNumber]
        navigationController?.pushViewController(aVC, animated: true)
    }
    
    @IBAction func previousSwiped(_ sender: UISwipeGestureRecognizer) {
        if topicsArray.count != 0 {
            topicNumber -= 1
            
            if topicNumber < 0 {
                topicNumber = 0
            } else {

                UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                    self.topicView.frame.origin.x = self.view.frame.size.width
                }, completion: { (finished: Bool) in
                    self.topicView.frame.origin.x = -self.view.frame.size.width
                    self.showTopics()
                    
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                        self.topicView.frame.origin.x = 0
                    }, completion: { (finished: Bool) in })
                })
            }

        }
    }
    
    @IBAction func nextSwiped(_ sender: UISwipeGestureRecognizer) {
        if topicsArray.count != 0 {
            topicNumber += 1
            
            if topicNumber > topicsArray.count - 1 {
                topicNumber = topicsArray.count - 1
            } else {
                UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                    self.topicView.frame.origin.x = -self.view.frame.size.width
                }, completion: { (finished: Bool) in
                    self.topicView.frame.origin.x = self.view.frame.size.width
                    self.showTopics()
                    
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                        self.topicView.frame.origin.x = 0
                    }, completion: { (finished: Bool) in })
                })
            }
        }
    }
    
    
    @IBAction func longGesture(_ sender: UILongPressGestureRecognizer) {
        if sender.state == .began
        {
            topicsArray = []
            queryTopics()
        }
    }
    
    
    
    
    
    
    func addTopic()
    {
        let alert = UIAlertController(title: "Topics",
                                      message: "Write a Topic",
                                      preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "Submit", style: .default, handler: { (action) -> Void in
            let textField = alert.textFields!.first!
            let txtStr = textField.text!
            
            if txtStr != "" {
                
                let txtStr2 = txtStr.replacingOccurrences(of: "?", with: "")
                let keywords = txtStr2.lowercased().components(separatedBy: " ")
                
                let topicData = ["Text":txtStr,"Comments":0,"Votes":0, "Keywords": keywords,"TopicReported":false,"TopicPointer":self.currentUser, "createdAt": ServerValue.timestamp()] as [String:Any]
                
                DataService.instance.REF_USERS.observeSingleEvent(of: .value, with: { (snapshots) in
                    
                    if let userSnapsot = snapshots.children.allObjects as? [DataSnapshot]
                    {
                        for user in userSnapsot
                        {
                            if user.key == Auth.auth().currentUser!.uid
                            {
                                if let userDict = user.value as? Dictionary<String, AnyObject>
                                {
                                    var userPoints = userDict["userPoints"] as! Int
                                    userPoints += 5
                                    let userKarma = ["userPoints": userPoints] as [String: Any]
                                    DataService.instance.createTopics(uid: self.currentUser, topicData: topicData, userData: userKarma)
                                }
                            }
                        }
                    }
                    
                })
                
            } else {
                
                self.showErrorAlert(_message: "You must type something!")
            }
        })
        
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        
        alert.addTextField { (textField: UITextField) in
            textField.keyboardAppearance = .dark
            textField.keyboardType = .default
            textField.autocorrectionType = .default
        }
        
        alert.addAction(ok)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    
    func queryTopics()
    {
        showHUD("Please Wait ..")
        
        DataService.instance.REF_TOPICS.queryOrdered(byChild: "createdAt").observe(.value) { (snapshots) in
            
            self.topicsArray = []
            if let topicSnapshot = snapshots.children.allObjects as? [DataSnapshot]
            {
                for topic in topicSnapshot
                {
                    if let topicDict = topic.value as? Dictionary<String,Any>
                    {
                        let reported = topicDict["TopicReported"] as! Bool
                        
                        if (!reported)
                        {
                            self.topicsArray.insert(topic, at: 0)
                        }
                        
                    }
                    
                }
                self.hideHUD()
                self.showTopics()
            }
        }
    }
    
    
    func showTopics()
    {
        
        if topicsArray.count != 0 {
            
            commentsOutlet.isEnabled = true
            if let topic = topicsArray[topicNumber].value as? Dictionary<String,Any>
            {
                let text = topic["Text"] as! String
                topicLbl.text = text
                let comment = topic["Comments"] as! Int
                if comment != 0
                {
                    commentsOutlet.setTitle( "\(comment)    comments", for: .normal)
                }
                else
                {
                    commentsOutlet.setTitle( "no comments yet", for: .normal)
                }
            }

        }
    }
    
    
}
