//
//  SignUpVC.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase
import OneSignal
import FirebaseStorage

class SignUpVC: UIViewController, Alertable {

    @IBOutlet weak var usernameField: DesginableField!
    @IBOutlet weak var pwdField: DesginableField!
    @IBOutlet weak var emailField: DesginableField!
    
    var defaultImage = UIImage(named: "user_avatar")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func readRulesTapped(_ sender: Any) {
        let aVC = storyboard?.instantiateViewController(withIdentifier: "TermOfUse") as! TermOfUse
        present(aVC, animated: true, completion: nil)
    }
    
    
    @IBAction func signUpTapped(_ sender: Any)
    {
        showHUD("Please Wait ..")
        
        guard let email = emailField.text, let password = pwdField.text, let username = usernameField.text, email != "" , username != "" , password != "" else {
            
            self.hideHUD()
            self.showErrorAlert(_message: "Please fill all fields")
            return }
        
        let status : OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        let userID = status.subscriptionStatus.userId
        
        var playerID = ""
        
        if userID != nil
        {
            playerID = userID!
        }
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            if error == nil
            {
                self.samplePhotoUrl(completion: { (photourl) in
                    let userData = ["userEmail":email, "username": username, "provider": user!.providerID, "userReported":false, "userPoints":0,"playerID": playerID, "UserProfileUrl": photourl, "createdAt": ServerValue.timestamp() ] as [String:Any]
                    AuthService.instance.createFirebaseUser(uid: user!.uid, userData: userData)
                    self.hideHUD()
                    self.dismiss(animated: true, completion: nil)
                })
            
            }
            else
            {
                self.hideHUD()
                self.showErrorAlert(_message: error!.localizedDescription)
            }
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    func samplePhotoUrl(completion: @escaping (String)->())
    {
        let data = UIImageJPEGRepresentation(defaultImage!, 0.8)
        let filePath = "\(Auth.auth().currentUser!.uid)/\("defaultImage")"
        Storage.storage().reference().child(filePath).putData(data!, metadata: nil) { (metaData, error) in
            guard let metaData = metaData else {
                print(error ?? "")
                return
                
            }
            
            let downloadUrl  = metaData.downloadURL()?.absoluteString
            completion(downloadUrl!)
        }
        
    }
    
    

}
