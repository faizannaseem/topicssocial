//
//  DataService.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import Foundation
import Firebase

class DataService
{
    static let instance = DataService()
    
    private let _REF_TOPICS = DB_BASE.child("topics")
    private var _REF_USERS = DB_BASE.child("users")
    private var _REF_COMMENTS = DB_BASE.child("comments")
    
    var REF_USERS : DatabaseReference
    {
        return _REF_USERS
    }
    
    var REF_TOPICS: DatabaseReference
    {
        return _REF_TOPICS
    }
    
    var REF_COMMENTS: DatabaseReference
    {
        return _REF_COMMENTS
    }
    
    func createTopics(uid: String, topicData : Dictionary<String, Any>, userData: Dictionary<String, Any>)
    {
        REF_TOPICS.childByAutoId().updateChildValues(topicData)
        REF_USERS.child(uid).updateChildValues(userData)
    }
    
    func createTopicComment(uid: String,commentData: Dictionary<String, Any>,userData: Dictionary<String, Any>, topicId: String)
    {
        REF_COMMENTS.child(topicId).childByAutoId().updateChildValues(commentData)
        REF_USERS.child(uid).updateChildValues(userData)
    }
    
    
}
