//
//  AuthService.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()

class AuthService
{
    static let instance = AuthService()
    
    private var _REF_USERS = DB_BASE.child("users")
    
    var REF_USERS : DatabaseReference
    {
        return _REF_USERS
    }
    
    func createFirebaseUser(uid: String, userData : Dictionary<String, Any>)
    {
        REF_USERS.child(uid).updateChildValues(userData)
    }
    
}
