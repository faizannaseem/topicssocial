//
//  Comment.swift
//  TopicsSocial
//
//  Created by Faizan on 23/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import Foundation

class Comment {
    
    private var _reported: Bool!
    private var _text: String!
    private var _userPointer: String!
    private var _votes: Int!
    private var _createdAt: TimeInterval!
    private var _profileUrl: String!
    private var _username: String!
    
    var reported: Bool
    {
        return _reported
    }
    
    var text: String
    {
        return _text
    }
    
    var userPointer: String
    {
        return _userPointer
    }
    
    var votes: Int
    {
        return _votes
    }
    
    var createdAt: TimeInterval
    {
        return _createdAt
    }
    
    var profileUrl : String
    {
        return _profileUrl
    }
    
    var username: String
    {
        return _username
    }
    
    init(reported: Bool, text: String, userPointer: String, votes: Int, createdAt: TimeInterval, profileUrl: String, username: String) {
        self._createdAt = createdAt
        self._reported = reported
        self._text = text
        self._userPointer = userPointer
        self._votes = votes
        self._profileUrl = profileUrl
        self._username = username
    }
    
    
}
