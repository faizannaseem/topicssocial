//
//  DesginableField.swift
//  TopicsSocial
//
//  Created by Faizan on 13/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

class DesginableField: UITextField {

    override func awakeFromNib() {
        setupView()
    }
    
    
    func setupView()
    {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!, attributes:[NSAttributedStringKey.foregroundColor: UIColor.white])
    }
    
}
