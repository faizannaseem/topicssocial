//
//  CommentCell.swift
//  TopicsSocial
//
//  Created by Faizan on 12/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class CommentCell: UITableViewCell {

    @IBOutlet weak var avatarImg: UIImageView!
    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var commentTxt: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var upButton: UIButton!
    @IBOutlet weak var votesLabel: UILabel!
    @IBOutlet weak var downButton: UIButton!
    @IBOutlet weak var optionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(comment: DataSnapshot)
    {
       
        if let commentDict = comment.value as? Dictionary<String,Any>
        {
            let commentText = commentDict["Text"] as! String
            let userPointer = commentDict["UserPointer"] as! String
            let timestamp = commentDict["createdAt"] as! TimeInterval
            let vote = commentDict["Votes"] as! Int
            
            commentTxt.text = commentText
            votesLabel.text = "\(vote)"
            let date = Date(timeIntervalSince1970: timestamp / 1000)
            let now = Date()
            let timeAgo = self.timeAgoSinceDate(date, currentDate: now, numericDates: true)
            
            
            DataService.instance.REF_USERS.child(userPointer).observeSingleEvent(of: .value, with: { (usershots) in

                if let userSnapshot = usershots.value as? Dictionary<String, Any>
                {
                    let username = userSnapshot["username"] as! String
                    let profileUrl = userSnapshot["UserProfileUrl"] as! String
                    
                    self.dateLabel.text = "\(username) • " + "\(timeAgo)"
                    self.avatarImg.sd_setImage(with: URL(string: "\(profileUrl)"), placeholderImage: UIImage(named: "user_avatar"), options: [.continueInBackground, .progressiveDownload], completed: nil)
                }

            })
            
            
        }
        
    }
    

}
