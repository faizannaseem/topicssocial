//
//  ShadedView.swift
//  TopicsSocial
//
//  Created by Faizan on 13/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

class ShadedView: UIView {

    
    override func awakeFromNib() {
        setupView()
    }
    
    func setupView()
    {
        self.layer.shadowOpacity = 0.3
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 5.0
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
    }
    

}
