//
//  RoundedButton.swift
//  TopicsSocial
//
//  Created by Faizan on 13/01/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    override func awakeFromNib() {
        setupView()
    }
    
    
    func setupView()
    {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
    
}
