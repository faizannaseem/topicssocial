//
//  UserActivityCell.swift
//  TopicsSocial
//
//  Created by Faizan on 08/02/2018.
//  Copyright © 2018 Faizan. All rights reserved.
//

import UIKit
import Firebase

class UserActivityCell: UITableViewCell {

    @IBOutlet weak var topicTitleLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func configureCell(activity: DataSnapshot)
    {
        if let activity = activity.value as? Dictionary<String,Any>
        {
            let text = activity["Text"] as! String
            topicTitleLabel.text = text
            let comment = activity["Comments"] as! Int
            commentLabel.text = "\(comment) Comments"
            
        }
    }
    
    
    
    
}
